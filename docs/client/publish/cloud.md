# 发行云端资源

本地开发调试后，务必上传到uniCloud服务空间才能在现网生效。

各种云函数、公共模块等都需要上传发行。

右键 cloudfunctions 目录，

![](https://cdn.fsq.pub/vkdoc/vk-client/1718278237754rr95efdt2bg.png)