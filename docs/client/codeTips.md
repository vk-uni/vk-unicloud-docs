---
sidebarDepth: 0
---

# 代码提示

## 代码块提示@codetips

代码块是很方便的代码提示工具，简单的敲几个字母，回车，就能生成大段代码。效果如下：

**效果**

在 `script` 内输入vk

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/3d2824ab-d034-48be-9fff-f49edde50921.png)

在 `云函数` 内输入dao.

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/fe965ad9-e9ed-44fc-ad0b-5bc286988e09.png)

**安装代码块步骤**

**下载 [VK框架快速开发辅助工具](https://ext.dcloud.net.cn/plugin?id=6663)**

安装好后重启下HBX，即可直接使用！

## d.ts 语法提示@dts

**效果**

可以清楚的看到某个方法可接受哪些参数，并且返回值是什么

![](https://cdn.fsq.pub/vkdoc/vk-client/1715786134138t96cpsvdqq8.png)

vk.navigateTo 也能自动提示页面了

![](https://cdn.fsq.pub/vkdoc/vk-client/171578624935168169cn3k3.png)

**安装代码块步骤**

**下载最新版 [VK框架快速开发辅助工具](https://ext.dcloud.net.cn/plugin?id=6663)**

安装好后重启下HBX，然后在任意页面右键，VK-开启VK框架d.ts语法提示（提示成功后，再重启HBX即可生效）

![](https://cdn.fsq.pub/vkdoc/vk-client/171604049333666si8c9fm4g.png)



