---
sidebarDepth: 0
---

# 更新教程

支付插件的更新非常简单，右键项目根目录下的 `uni_modules/vk-uni-pay` 目录，再点击【从插件市场更新】，如下图所示。

![](https://cdn.fsq.pub/vkdoc/vk-pay/d80275f3-40f1-4ea0-8b81-5009324a1fd6.png)

在弹出的窗口中，按下图所示操作。

![](https://cdn.fsq.pub/vkdoc/vk-pay/bbfcf1d3-698d-4ac1-920a-8df3c7ebb633.png)

更新完后需要上传公共模块和云函数才能生效，如下图所示。

![](https://cdn.fsq.pub/vkdoc/vk-pay/babb34f0-357d-419f-bc3a-394125c25c74.png)

## 注意事项

* 更新后，需要在 `common/vk-uni-pay` 右键上传公共模块以及再上传下 `vk-pay` 云函数才会生效

* 若是本地调试模式，如果不生效，尝试重启项目。