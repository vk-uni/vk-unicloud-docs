---
sidebarDepth: 0
---

# 查询付款状态

## vkPay.queryPayment

## 示例代码@demo

无框架下的云函数代码示例（该写法同时也适用于任何框架）

```js
const vkPay = require("vk-uni-pay");

exports.main = async (event, context) => {

  let res = await vkPay.queryPayment({
    out_trade_no: "商户支付订单号", 
    await_notify: false, // 是否需要等待异步通知执行完成，若为了响应速度，可以设置为false，若需要等待异步回调执行完成，则设置为true
    pay_order_info: false, // 是否需要返回支付订单信息，默认为false
  });

  return res;
};
```

## 请求参数@params

| 参数   | 说明       | 类型    | 默认值  | 可选值 |
|------- |-----------|---------|-------|-------|
| out_trade_no  |   必填项，商户支付订单号，需自行保证全局唯一    | String  | -    | -  |
| await_notify  |   是否需要等待异步通知执行完成后才返回给前端支付结果   | Boolean  | false  | true  |
| await_max_time  |   最大等待时长，默认20秒（单位秒）   | Number  | 20  | 范围 5-40  |
| pay_order_info  |   是否需要返回支付订单信息  | Boolean  | false  | true  |

### 主动执行回调函数@params-await_notify

> vk-pay的版本需 >= 1.14.0

`vkPay.queryPayment` 若设置参数 `await_notify: true`，则等待0.5秒后如果还未接收到支付公司推送的异步通知，则主动执行自定义回调函数的逻辑，可做到即使支付公司的异步通知延迟了几分钟，甚至未发异步通知，也能正常响应支付成功的情况。[异步回调说明](https://vkdoc.fsq.pub/vk-uni-pay/uniCloud/pay-notify.html)
 
### await_notify

详细说明

以支付宝为例，整个支付环节大致会经过这几个步骤

* 1、用户在支付宝app输入支付密码
* 2、支付宝异步回调开发者服务器（云函数）
* 3、云函数接受到异步通知后，准备执行自己业务系统逻辑（比如给用户充值余额、或修改订单状态为已支付等等）
* 4、前端响应（如用户看到余额增加了或订单变成已支付了）
* 5、完成

上面的步骤中，第3步和第4步的顺序不一定谁先谁后，为了确保顺序一定是3前4后，则设置 `await_notify` 为 `true` 即可（一般这种情况的场景是前端页面数据需要通过数据库查询获得，因此需要等异步回调更新数据库数据完成后才行）

但是如果你想让前端更快的获得结果（比如不管异步回调执行是否完成，前端都显示支付成功，则设置 `await_notify` 为 `false` 可以加快响应速度，但此时自定义回调函数只依赖异步回调执行）

## 返回值@return

|参数名							|类型		|说明																																																																						|
|:-:								|:-:		|:-:																																																																						|
|orderPaid					|Boolean|标记用户是否已付款成功（此参数只能表示用户确实付款了，但系统的异步回调逻辑可能还未执行完成）																										|
|user_order_success	|Boolean|用户异步通知逻辑是否全部执行完成，且无异常（参数await_notify为true时此值才能正常返回，此时建议前端可以通过此参数是否为true来判断是否支付成功）	|
|out_trade_no				|String	|支付插件订单号																																																																	|
|transaction_id			|String	|第三方支付交易单号（只有付款成功的才会返回）																																																		|
|status							|Number	|当前支付订单状态 -1：已关闭 0：未支付 1：已支付 2：已部分退款 3：已全额退款																																		|
|payOrder						|Object	|支付订单完整信息（参数pay_order_info为true时才会返回此值）																																											|

如果不需要等待异步回调，则直接通过 orderPaid 来判定是否支付即可

```js
if (orderPaid) {
  // 用户已付款
} else {
  // 用户未付款
}
```

如果需要确保异步回调执行成功，则请求参数需要传 `await_notify:true`，此时判断逻辑如下

```js
if (orderPaid) {
  if (user_order_success === true) {
    // 用户付款成功，且已接收到异步回调，同时自定义回调逻辑也执行成功
    // 可以给用户提示支付成功了
  } else if (user_order_success === false) {
    // 用户付款成功，且已接收到异步回调，但自定义回调逻辑执行失败
    // 此时需要提醒用户支付出现了故障，请联系客服处理
  } else if (status === 0) {
    // 用户付款成功，但还未收到异步回调
    // 这种情况很少见，代表异步回调都没收到，需要提醒用户支付出现了故障，请联系客服处理
  } else {
    // 用户付款成功，且已接收到异步回调，但自定义回调逻辑还在执行中
    // 这种情况一般是你的自定义回调逻辑运行时间太长了，还没运行完导致的，建议优化自定义回调逻辑
  }
} else {
  // 用户未付款
}
```
