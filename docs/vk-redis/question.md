# 常见问题

## 云函数与Redis的连接@q1

和传统开发不同，云函数实例之间是不互通的，也就是说每个使用Redis的云函数实例都会和Redis建立一个连接，插件内置了连接池，在云函数实例复用时Redis连接也会复用。

## 云函数本地调试@q2

支持云函数本地调试。
