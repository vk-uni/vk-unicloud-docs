module.exports = [
  { text: '指南', link: '/' },
  { text: 'client端框架', link: '/client/' },
  { text: 'admin端框架', link: '/admin/' },
  { text: 'vk-uni-pay（统一支付）', link: '/vk-uni-pay/' },
  { text: 'Redis扩展', link: '/vk-redis/' },
  { text: '数据库一键搬家', link: '/db-migration/' },
  {
    text: 'DCloud插件市场',
    link: 'https://ext.dcloud.net.cn/publisher?id=1804602',
    ariaLabel: 'DCloud插件市场',
    items: [
      { text: 'vk-unicloud-router（client端）', link: 'https://ext.dcloud.net.cn/plugin?id=2204' },
      { text: 'vk-unicloud-admin（admin端）', link: 'https://ext.dcloud.net.cn/plugin?id=5043' },
      { text: 'vk-uni-pay（统一支付）', link: 'https://ext.dcloud.net.cn/plugin?id=5642' },
      { text: 'vk-data-goods-sku-popup（sku组件）', link: 'https://ext.dcloud.net.cn/plugin?id=2848' },
      { text: 'vk-uview-ui（uView Vue3.0）', link: 'https://ext.dcloud.net.cn/plugin?id=6692' },
      { text: 'vk-unicloud数据库一键搬家工具', link: 'https://ext.dcloud.net.cn/plugin?id=6089' },
      { text: 'vk-hbx插件（开发辅助工具）', link: 'https://ext.dcloud.net.cn/plugin?id=6663' },
      { text: 'vk-mail（邮件发送公共模块）', link: 'https://ext.dcloud.net.cn/plugin?id=7688' },
      { text: 'vk-mall（vk商城client端）', link: 'https://ext.dcloud.net.cn/plugin?id=9502' },
      { text: 'vk-mall-admin（vk商城admin端）', link: 'https://ext.dcloud.net.cn/plugin?id=9504' },
      { text: 'vk框架成品项目（含群内成员作品）', link: 'https://ext.dcloud.net.cn/search?q=vk&orderBy=HotList&cat1=7&cat2=72' },
      { text: '查看更多', link: 'https://ext.dcloud.net.cn/publisher?id=1804602' }
    ]
  },
  {
    text: '关联技术文档',
    link: '',
    ariaLabel: '关联技术文档',
    items: [
      { text: 'uni-app', link: 'https://uniapp.dcloud.io/' },
      { text: 'uniCloud', link: 'https://uniapp.dcloud.io/uniCloud/' },
      { text: 'Vue2', link: 'https://cn.vuejs.org/index.html' },
      { text: 'Vue3', link: 'https://staging-cn.vuejs.org/' },
      { text: 'vk-uview', link: 'https://vkuviewdoc.fsq.pub' },
      { text: 'vkmall', link: 'https://vkmalldocs.fsq.pub' }
    ]
  },
  {
    text: 'AI小助手',
    link: '',
    ariaLabel: 'AI小助手',
    items: [
      { text: 'VK小助手之云开发', link: 'https://chatglm.cn/main/gdetail/65ddd9d832f609421a9c719c?lang=zh' },
      { text: 'VK小助手之前端开发', link: 'https://chatglm.cn/main/gdetail/65fd3f408da5f6f0bb6c739a?lang=zh' },
      { text: 'VK小助手之uni-app专题', link: 'https://chatglm.cn/main/gdetail/67b6a3a1b75c98fc90327f59?lang=zh' },
    ]
  },
  {
    text: 'Gitee',
    ariaLabel: 'Gitee',
    items: [
      { text: 'vk-unicloud-router（client端）', link: 'https://gitee.com/vk-uni/vk-uni-cloud-router' },
      { text: 'vk-unicloud-admin（admin端）', link: 'https://gitee.com/vk-uni/vk-unicloud-admin' },
      { text: 'vk-unicloud-docs（文档仓库）', link: 'https://gitee.com/vk-uni/vk-unicloud-docs.git' }
    ]
  }
]
